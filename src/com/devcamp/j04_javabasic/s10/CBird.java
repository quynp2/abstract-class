package com.devcamp.j04_javabasic.s10;

public class CBird extends CPet implements IFlyable {

    @Override
    public void fly() {
        // TODO Auto-generated method stub
        System.out.println("Bird can Fly");
    }

    public void print() {
        System.out.println("Bird Print...");

    };

    public void play() {
        System.out.println("Bird Play...");

    }

    public void animalSound() {
        // TODO Auto-generated method stub
        System.out.println("Bird Sound...");

    }

}
