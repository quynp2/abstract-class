package com.devcamp.j04_javabasic.s10;

public class CPet extends CAnimal {

    protected int age;
    protected String name;

    @Override
    public void animalSound() {
        // TODO Auto-generated method stub
        System.out.println("Pet Sound...");

    }

    @Override
    public void eat() {
        System.out.println("Pet eating...");

    }

    public void print() {
        System.out.println("Pet Print...");

    };

    public void play() {
        System.out.println("Pet Play...");

    }

    public static void main(String[] args) {
        CPet pet = new CPet();
        pet.animclass = AnimalClass.birds;
        pet.name = "LULU";
        pet.age = 3;
        pet.animalSound();
        pet.eat();
        pet.print();
        pet.play();

        pet = new CFish();
        pet.animalSound();
        pet.eat();
        pet.print();
        pet.play();
        ((CFish) pet).swim();

        pet = new CBird();
        pet.animalSound();
        pet.eat();
        pet.print();
        pet.play();

        CFish fish = new CFish();
        fish.name = "MONO";
        fish.age = 3;
        fish.animclass = AnimalClass.fish;
        fish.animalSound();
        fish.eat();
        fish.print();
        fish.play();
        fish.swim();

    }

}
