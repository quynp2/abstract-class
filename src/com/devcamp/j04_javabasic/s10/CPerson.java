package com.devcamp.j04_javabasic.s10;

import java.util.ArrayList;

public class CPerson extends CAnimal {
    private int id;
    private int age;
    private String firstname;
    private String lastname;
    private ArrayList<CPet> pets;

    // HAM KHOI TAO
    public CPerson() {
    }

    public CPerson(int age) {
        this.age = age;
    }

    public CPerson(int id, int age, String firstname, String lastname, ArrayList<CPet> pets) {
        this.id = id;
        this.age = age;
        this.firstname = firstname;
        this.lastname = lastname;
        this.pets = pets;
    }

    // PHUONG THUC GETTER SETTER
    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public ArrayList<CPet> getPets() {
        return pets;
    }

    public void setPets(ArrayList<CPet> pets) {
        this.pets = pets;
    }

    @Override
    public String toString() {
        return "CPerson {\"id\":" + this.id + ", age:" + age + ", firstname: " +
                firstname + ", lastname:" + lastname + ", pet:" + this.pets;
    }

    @Override
    public void animalSound() {
        // TODO Auto-generated method stub
        System.out.println("cperson");
    }

}
