package com.devcamp.j04_javabasic.s10;

public class CFish extends CPet implements ISwimable {

    @Override
    public void swim() {
        // TODO Auto-generated method stub
        System.out.println("Fish Swimming");
    }

    @Override
    public void eat() {
        System.out.println("Fish eating...");

    }

    public void print() {
        System.out.println("Fish Print...");

    };

    public void play() {
        System.out.println("Fish Play...");

    }

    public void animalSound() {
        // TODO Auto-generated method stub
        System.out.println("Fish Sound...");

    }

}
