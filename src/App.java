import java.util.ArrayList;

import com.devcamp.j04_javabasic.s10.CAnimal;
import com.devcamp.j04_javabasic.s10.CBird;
import com.devcamp.j04_javabasic.s10.CCat;
import com.devcamp.j04_javabasic.s10.CFish;
import com.devcamp.j04_javabasic.s10.CPerson;
import com.devcamp.j04_javabasic.s10.CPet;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        CAnimal name01 = new CFish();
        CPet name02 = new CBird();
        name02.animalSound();
        name02 = new CFish();
        name02.animalSound();
        name02 = new CCat();
        name02.animalSound();

        CPerson person = new CPerson();
        person.setAge(18);
        ArrayList<CPet> petsList = new ArrayList<>();
        petsList.add(name02);
        petsList.add((CPet) name01);
        person.setPets(petsList);
        System.out.println(person);
    }
}
